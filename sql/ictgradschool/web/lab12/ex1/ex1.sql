-- Answers to exercise 1 questions
SELECT DISTINCT dept
FROM unidb_courses; -- A

SELECT DISTINCT semester
FROM unidb_attend; -- B

SELECT a.id, c.descrip
FROM unidb_attend AS a,
     unidb_courses AS c
WHERE a.num = c.num
  AND a.dept = c.dept; -- C

SELECT CONCAT(fname, ' ', lname) AS name, country
FROM unidb_students
ORDER BY fname;

SELECT CONCAT(fname, ' ', lname) AS name, mentor
FROM unidb_students
ORDER BY mentor;

SELECT CONCAT(fname, ' ', lname) AS name, office
FROM unidb_lecturers
ORDER BY office;

SELECT CONCAT(fname, ' ', lname) AS name
FROM unidb_lecturers
WHERE staff_no > 500;

SELECT CONCAT(fname, ' ', lname) AS name
FROM unidb_students
WHERE id BETWEEN 1668 AND 1824;

SELECT CONCAT(fname, ' ', lname) AS name
FROM unidb_students
WHERE country = 'NZ' or country = 'AU' or country = 'US';

SELECT CONCAT(fname, ' ', lname) AS name
FROM unidb_lecturers
WHERE office like 'G%';

SELECT descrip
FROM unidb_courses
WHERE dept != 'comp';

SELECT CONCAT(fname, ' ', lname) AS name
FROM unidb_students
WHERE country = 'FR' or country = 'MX';