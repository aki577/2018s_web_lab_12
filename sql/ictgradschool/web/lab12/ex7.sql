DROP TABLE IF EXISTS web12ex07_player;
DROP TABLE IF EXISTS web12ex07_team;
DROP TABLE IF EXISTS web12ex07_league;

CREATE TABLE IF NOT EXISTS web12ex07_league(
  name VARCHAR(20) PRIMARY KEY
);

CREATE TABLE IF NOT EXISTS web12ex07_team(
  id INT NOT NULL PRIMARY KEY ,
  name VARCHAR(20),
  homeCity VARCHAR(20),
  captain VARCHAR(50),
  points INT NOT NULL,
  leagueName VARCHAR(20),
  FOREIGN KEY (leagueName) REFERENCES web12ex07_league(name)
);

CREATE TABLE IF NOT EXISTS web12ex07_player(
  id INT NOT NULL PRIMARY KEY ,
  fName CHAR(20),
  lName CHAR(20),
  nationality VARCHAR(20),
  age INT(3),
  teamId INT NOT NULL,
  FOREIGN KEY (teamId) REFERENCES web12ex07_team(id)
);


-- ALTER TABLE web12ex07_player ADD CONSTRAINT teamId FOREIGN KEY (teamId) REFERENCES web12ex07_team(id);

INSERT INTO web12ex07_league
VALUES ('Soccer League');

INSERT INTO web12ex07_team
VALUES (1, 'Yay', 'Hamilton', 'John Smith', 20, 'Soccer League'),
       (2, 'Hooray', 'Washington D.C.', 'Joe Tnfsad', 15, 'Soccer League');

INSERT INTO web12ex07_player
VALUES (11, 'John', 'Smith', 'NZ', 26, 1),
       (12, 'Sam', 'Fsfdaf', 'US', 28, 1),
       (21, 'Joe', 'Tnfsad', 'FR', 32, 2);
