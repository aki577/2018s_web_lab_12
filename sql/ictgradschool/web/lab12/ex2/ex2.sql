-- Answers to exercise 2 questions
SELECT DISTINCT CONCAT(n.fname, ' ', n.lname) AS name
FROM unidb_students as n, unidb_attend as a
WHERE n.id = a.id AND a.dept = 'comp';


SELECT CONCAT(s.fname, ' ', s.lname) as Name
FROM unidb_students as s, unidb_courses as r
WHERE s.id = r.rep_id and s.country != 'NZ';


SELECT l.office
FROM unidb_lecturers AS l, unidb_teach AS t
WHERE l.staff_no = t.staff_no AND t.num = 219;


SELECT CONCAT(fname, ' ', lname) as Name
FROM unidb_students
WHERE id IN (SELECT DISTINCT id
             FROM unidb_attend
             WHERE dept = 'comp'
               AND num IN (SELECT num
                             FROM unidb_teach AS t, unidb_lecturers AS l
                             WHERE t.staff_no = l.staff_no
                               AND l.fname = 'Te Taka'));


SELECT CONCAT(s.fname, ' ', s.lname) AS name, CONCAT(m.fname, ' ', m.lname) as mentorname
FROM unidb_students as s, unidb_students as m
WHERE m.id = s.mentor;


SELECT CONCAT(fname, ' ', lname) AS LecturerName
FROM unidb_lecturers
WHERE office = 'G%'
UNION
SELECT CONCAT(fname, ' ', lname) as StudentName
FROM unidb_students
WHERE country != 'NZ';


SELECT CONCAT(c.dept,'', c.num) AS Course, CONCAT(l.fname, ' ', l.lname) AS CoordinatorName, CONCAT(s.fname, ' ', s.lname) AS RepStudentName
FROM unidb_courses as c, unidb_students as s, unidb_lecturers as l
WHERE c.dept = 'comp' AND c.num = '219' AND c.coord_no = l.staff_no AND c.rep_id = s.id;
